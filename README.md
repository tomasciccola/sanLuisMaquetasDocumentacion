# Sistema de control de maquetas 

Este repo documenta el control de la Maqueta Programable SL 4.0

## Rangos de ips

Todos los dispositivos tienen IP estática

```
10.75.33.
          .100 a 120   --> Para uso de maqueta (arduinos y raspis)

```

255.255.255.192 --> Máscara de subred

10.75.33.65 --> Gateway

10.16.64.10 --> DNS


10.75.33.101 ---> Ctrl de maqueta 

        .102 ---> Pantalla 2 (HDMI FUTURE)

        .103 ---> Pantalla 1 (HDMI ESTADIO)

        .104 ---> Pantalla 3 (SAN LUIS)
        .111 ---> arduino tablero 1
        .112 ---> arduino tablero 2
        .113 ---> arduino tablero 3
        .114 ---> arduino tablero 4
        .115 ---> arduino tablero 5
        

## WEB DE CONTROL
En el mismo servidor de control se corre una web estática que sirve para controlar la web localmente. La misma se entrega a través de la red oculta:

ssid: "srvmaqueta"
pass: "plan2019"

(sin comillas)

Se accede a través de la IP 192.168.4.1

Puede ser que también se pueda acceder desde la ip del servidor de la maqueta, aunque esto no debería suceder

Para empezar a usarla hay que hacer click en el boton "Activar control". En el momento en que se active se cortarán todos los mensajes que lleguen de la web externa
También se puede activar el modo automático que genera mensajes de control aleatorios automáticamente



## Especificación de API
https://docs.google.com/spreadsheets/d/1_pbgRCZ50kuIppiZdXA-gqMqnVDNAiMAF3_LyDvcuRs/edit#gid=1559493658

en las columnas L y M se pueden ver los comandos de la API de cada uno de los objetos programables y los parámetros de cada uno

Api web (PUERTO:8001)

/accionMaqueta/:modulo/:zona/:efecto/:control

devuelve --> {"msg":"OK"}
             {"msg":"UNAVAILABLE"} --> en el caso del modo demo


## Especificaciones del Servidor

Servidor hecho con nodejs

Comunicación con Maqueta:

Se comunica vía TCP con cada dispositivo (raspi/arduino) con mensajes en bytes.
Este servicio está alojado en la misma compu que el front/back de la web. Hay otro server físicamente en el lugar (una raspberry pi) a modo de repetidor.
No sólo recibe requests de la web sino también enruta mensajes intra dispositivos.

Comunicación con el frontend:

El servidor tiene una API que se conforma en relación a la especificación del documento previo (https://docs.google.com/spreadsheets/d/1_pbgRCZ50kuIppiZdXA-gqMqnVDNAiMAF3_LyDvcuRs/edit#gid=1559493658)

Un esquema fácil a partir de esto sería una API del tipo

“/accionMaqueta/:módulo/:zona/:efectoDeProgramación/:paramétro”

Aclaración:
Los módulos son números enteros
La zona es un nombre corto
El efecto de programación también es un número que puede ir de 0 a nEfectos en zona.
Ejemplo: El control de mover el arbol responde al endpoint:

/accionMaqueta/4/merlo/3/1


Como el server de control no es accesible remotamente, el servidor del backend deberá oficiar a modo de repetidor de los requests del frontend.

El servidor de control sólo envía los mensajes que le llegan. No tiene lógica de tiempos. No manda mensajes automáticamente. Sólo manda los mensajes que recibe de la web. Toda la lógica de control (para que no se manden muchos mensajes, para que se apaguen módulos automáticamente en función de un timer, gestionar la cola de usuarios, etc) tienen que ser implementadxs desde la lógica del cliente web. 

## TIPOS DE DISPOSITIVOS

### Servidor de control de maquetas
 
Recibe GETS del backend de la web y mande mensajes TCP a los arduinos (control de luces y dispositivos) y otras raspis (pantallas con video).

API Web backend --> (HTTP) Servidor de Control (TCP) --> raspis y arduinos

Toda la data en el repo [ESTE](https://gitlab.com/tomasciccola/SanLuisMaquetasCtrlServer)


### Cliente reproductor de imágenes
Se conecta al repetidor. Cada raspi parsea un mensaje tcp concreto y muestra la imagen correspondiente. En el startup de estas raspis se tendrá que activar la salida de la pantalla así como correr el cliente tcp

fbcp-ili9341 es el soft/módulo del kernel que permite correr imagen en la pantallita SPI.
[Repo del módulo](https://github.com/juj/fbcp-ili9341)

La data en el repo [ESTE](https://gitlab.com/tomasciccola/sanLuisMaquetaCrtlPantalla)

## Repositorio con los programas de los Arduinos y documentación electrónica

[Programas de los Arduinos de los tableros](https://gitlab.com/martinpilu/maqueta-programable-4-san-luis/tree/master)


## Extras
[Backup Raspi](https://www.raspberrypi.org/documentation/linux/filesystem/backup.md)

En el archivo 3D.zip que está en este repositorio están todos los modelos 3d que se imprimieron para las diferentes partes de la maqueta. 

